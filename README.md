# A Concise Introduction to Git for Academics #

Version control systems provide great advantages for day-to-day
academic work, both for scientific collaboration and for teaching.  We
suggest git as the system of choice: it is _distributed_ and has
become the de-facto standard in this domain.  Git is also very
flexible, powerful, and can be used in many different workflows.
While there are excellent tutorials and other forms of online
documentation, few resources specifically address the needs of
academics.  This document aims at filling this gap, giving a concise
description of two specific workflows: collaborative projects within a
small group of researchers and in-class use in the setting of a
computational lab course.

### First time ###

* [How to install and configure git](FirstTime.md)
* [Set up the repository if you are the project lead](SetUp.md)

### Using git within a small group of scientific collaborators ###

* [Clone the repository](GroupClone.md)
* [The standard work cycle](GroupWorkCycle.md)
* A few more [things you should know](ShouldKnow.md)

### Using git in a computational lab course ###

* [Set up steps for the Instructor and Teaching Assistant](SetupInstructor.md)
* [Set up steps for students](SetupStudents.md)
* [The standard work cycle for students](ClassWorkCycle.md)

### Using git together with Overleaf ###

* [Some general thoughts on purpose and workflow](Overleaf.md)
* [Set up Overleaf/git when you are the project lead](OverleafStart.md)
* [Set up git if you are a project member](OverleafMember.md)

------

### Further reading ###

* [Understanding git
  conceptually](http://www.sbf5.com/~cduan/technical/git/), an excellent
  general introduction to git by Charles Duan
* [Git for scientists](https://neurathsboat.blog/post/git-intro/) -
  easy-to-read motivation and basic introduction to git for scientists
* [Version control with Git:
  resources](http://www.fperez.org/py4science/git.html), a blog post 
  specifically aimed at scientists with a number of useful links by
  Fernando Perez
* [The git project website](http://git-scm.com/) with comprehensive
  manuals and download instructions

------

### About this document ###

The pages in this repository are written and maintained by [Marcel
Oliver](http://math.jacobs-university.de/oliver/).  They are made
available under the Creative Commons [Attribution-ShareAlike 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/)
License.  I am happy to include and attribute fixes and improvements
to this document provided they help to make the document more concise
or more correct.

