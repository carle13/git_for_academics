## Clone the git project if you are a project member ##

The following needs to be done only once, by each project member.

### If you have received a Bitbucket invitation ###

1. If you have received an invitation to Bitbucket and want to use git
   directly, make sure [you have installed and configured git](FirstTime.md).

2. Follow the instructions provided in the invitation.  The essential
   step is (using the address provided by Bitbucket):

        git clone git@bitbucket.org:XXXX

3. You can now follow the [standard work cycle](GroupWorkCycle.md).
   All communication goes to Bitbucket, you cannot communicate to
   Overleaf directly.  Presumably, one person in the project will
   coordinate pushing and pulling to Overleaf.

### If you have received an invitation from Bitbucket and Overleaf ###

1. If you primarily want to use the Bitbucket repository, you should
   clone the Bitbucket version as described above.

2. Overleaf as an additional remote instead (Use the address given by
   Overleaf in its suggested `git clone` line):

        git remote add overleaf https://git.overleaf.com/XXXX

3. You can now follow the [standard work cycle](GroupWorkCycle.md).
   Note that `git push` and `git pull` will go to Bitbucket while
   `git push overleaf master` and `git push overleaf master` will go to
   Overleaf.


### If you have received only an Overleaf invitation ###

1. Follow the instructions in the Overleaf invitation.

2. Once logged into Overleaf, you can go to "Menu -> Sync -> Git"
   which provides a command line to clone the Overleaf repository to
   your computer; it looks like

        git clone https://git.overleaf.com/XXXX

3. In this case, overleaf is your default remote and you can follow
   the [standard work cycle](GroupWorkCycle.md) literally.  Note,
   however, that Overleaf as remote does not accept any branches other
   than the default branch `master`.