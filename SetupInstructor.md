## Setup instructions for Instructors and Teaching Assistant ##

It is assume that you have already [installed and configured
git](FirstTime.md) and [set up the central course
repository](SetUp.md), and [cloned the course repository to your local
machine](GroupClone.md).  For the Instructor and Teaching Assistant,
it is extremely helpful to have a graphical repository browser on the
local computer, as each student will be tracked as a branch of the
repository. 

### Message to students

Instruct students to clone your repository and to give write access to
you and, if applicable, to all Teaching Assistants and Graders.

### Change your "push default" for the class repository

* Inside the class repository, issue the following command

        git config push.default upstream

  (This will allow you to issue updates to the students with a simple
  `git push` once the remote repositories are configured as described below.)

### For each student, do the following

* Follow the link in the invitation email and copy the repository's
  SSH URL which can be found in the top right corner.

* (Optionally): add yourself as a "watcher" of their repository by
  clicking the "eye" symbol next to the URL box.

* On your computer, add the student repository as a "remote".  E.g.

        git remote add student_name git@bitbucket.org:name/repo.git

   (`student_name` is the name of your local branch for this student,
   the repository address is the one you have copied from bitbucket.)

* Create a local branch for the student:

        git remote update
        git checkout -b student_name student_name/master

### Useful commands

* Get back to the `master` branch (the branch with the official course
  material):

        git checkout master

* Get all new work from a student:

        git checkout student_name
        git pull

* Download new work for all students:

        git remote update

    (This does not pull the changes into your local branch for the
    student, but the work can already be viewed with a repositiory
    browser such as gitg.)
