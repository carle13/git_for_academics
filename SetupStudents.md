## How to configure your repository ##

After you have [installed and configured git](FirstTime.md), you need
to configure the course repository.

### First step: fork the repository on the server

1. Your instructor will have given you a URL to the course repository
   on bitbucket.  Visit the bitbucket page in a web browser, log into
   your bitbucket account, and fork the repository using the
   bitbucket web interface: "+"-Icon on the left, then "Fork this
   repository". 

2. Make sure you mark your clone of the repository _private_.

3. Go to "setting" (cog-wheel icon), then "User and group access".
   Add the instructor's bitbucket account and any additional accounts
   (e.g. for Teaching Assistants or Graders) with _write_ access.

4. (Optional): Add yourself, using the web interface on bitbucket, as
   a "watcher" to the instructor's repository.  You will then receive
   an email every time the instructor makes changes to this
   repository.

### Second step: clone the repository to your computer

1. In the terminal, navigate to the directory you wish to use as the
   parent folder for your local copy of the repository, then type
   (using the repository URL of _your_ forked version of
   the repository):

        git clone ssh://git@bitbucket.org/username/myrepo.git

2. To enter the repository root directory, type (adapting the name as
   necessary):

        cd myrepo

3. Add the instructors repository as a "remote":

        git remote add instructor <URL_to_instructor_repo>

    (fill in the correct URL for the instructor's bitbucket
    repository).  When the instructor makes updates to the repository
    in the future, you can then "pull" them into your repository saying

        git pull instructor master

You are now set up to start [working with your repository](ClassWorkCycle.md).
