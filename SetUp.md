## Set up the repository if you are the project lead ##

It is easiest to create the repository _on the server_.  Log into your
Bitbucket account and use the web-based repository creation tool.  This
tool will walk you through the necessary steps.

### For a small group of scientific collaborators ###

* Choose _private repository_ (the default)
* Go to "setting" (cog-wheel icon), then "User and group access".  Add
  all members of your team to "Users" list with _write_ access.  If
  they already have a Bitbucket account, you can add them directly.
  Otherwise enter their email address and they'll receive an
  invitation from Bitbucket.

### For use in a computational lab course ###

* Choose _public repository_ unless you really want to keep the class
  material private.  (If that is the case, you'll need to give
  explicit _read_ access to each student as decribed above.)
* Optional: add the teaching assistant(s) to "Users" with _write_
  access as described above.  (If all they need to do is grading, they
  won't need read access, but then they cannot contribute to the class
  material on the fly.)

### Finish up ###

* Note the repository SSH address on the "Overview" page (Note:
  currently, you need to click "I'm starting from scratch" to see the
  SSH address!)
* [Clone the repository as described here](GroupClone.md)

